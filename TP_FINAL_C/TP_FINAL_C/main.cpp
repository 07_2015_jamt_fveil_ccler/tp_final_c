/*AJOUT DES HEADERS*/
#include <iostream>
using namespace std;

#include <conio.h>				// Pour _khbit() et _getch()
#include <time.h>				// Pour time(int)
#include <stdlib.h>				// Pour srand(int) et rand()
#include "RockUtiles.h"			// Pour la gestion de l'ecran

/* NORMES DE CODAGE ET CONVENTIONS � UTILISER : */
// - TOUT LES MOTS D'UNE VARIABLE SONT S�PAR�S GR�CE AU CAMELCASE ET DOIVENT COMMENCER PAR UNE MINUSCULE 
//( CeciEstUnExempleDeCamelCase )
// - TOUTES LES VARIABLES DOIVENT �TRE D�CLAR�S AU D�BUT DE L'ENTR�E D'UNE FONCTION
// - TOUT LES BOOL�ENS DOIVENT S'APELLER " isQQCHOSE "( isANumber, isGreaterThan, isColliding, isAnArray etc.. )
// - LES CONSTANTES DOIVENT �TRE EN MAJUSCULES ET SONT S�PAR�ES GR�CE � UN _ ( UNDERSCORE )
// - UNE INDENTATION EST N�C�SSAIRE LORSQU'UNE LIGNE D�PEND DE LA PR�C�DENTE POUR QU'ELLE PUISSE SE REALISER
// - LES COMMENTAIRES SE FONT AU DESSUS DE LA LIGNE DE CODE
// ( Condition, boucle etc. )

/*D�CLARATION DES CONSTANTES*/
const int NB_LIGNES = 30;
const int NB_COLONNES = 70;
const int TAILLE_MAX = 31;

/*D�CLARATION DES FONCTIONS */
// PARTIE 1
void afficher_terrain(int NbLignes, int NbColonnes);
int recuperer_touche();
int calculer_direction_touche(int touche);
void position_aleatoire(int NbLignes, int NbColonnes, int &posX, int &posY);
void deplacer_serpent_I(int direction, int &posX, int &posY);
void attendre(int millisecondes);

// PARTIE 2
int saisir_niveau();
void creer_souris(int nbLignes, int NbColonnes, int sourisX[], int sourisY[], int nbSouris);
void afficher_souris(int sourisX[], int sourisY[], int nbSouris);
void deplacer_serpent_II(int direction, int serpentX[], int serpentY[], int &indiceTete, int &indiceQueue, int sourisX[], int sourisY[], int &nbSouris);
bool tester_collision(int X, int Y, int sourisX[], int sourisY[], int &nbSouris);

// FONCTIONS SUPPL�MENTAIRES
int calcul_delai(int nbSouris);
void afficher_mur();
int tirage_aleatoire(int max, int min);
void afficher_resultat_final(bool collisionMur, bool collisionQueue, int nbSouris);
void afficher_serpent(int serpentX[], int serpentY[], int indiceTete, int indiceQueue);
bool collision_mur(int X, int Y);
bool collision_queue(int X, int Y, int serpenX[], int serpentY[]);

/* D�FINITION DES FONCTIONS */

// La fonction suivante sert � afficher un terrain, en prennant le nombre de ligne et le nombre de colonnes de celui ci comme param�tre
void afficher_terrain(int NB_LIGNES, int NB_COLONNES) {

	/*cadre(Position X du debut, Position Y du debut, Nombre de colonnes, Nombre de lignes, Code couleur)*/
	cadre(0, 0, NB_COLONNES, NB_LIGNES, 4);
	cout << endl;

	return;
}

// La fonction sert a recuperer le code ASCII d'une touche pr�ss�e
int recuperer_touche() {

	/*D�CLARATION ET INITIALISATION DES VARIABLES*/
	int touche = -1;
	bool isTyping = false;

	do {
		// On v�rifie que l'utilisateur a pr�ss�e une touche gr�ce � la fonction _kbhit
		if (_kbhit()) {

			// On r�cup�re le caract�re saisie gr�ce � la fonction _getch
			touche = _getch();
			isTyping = true;
		}
		// On retourne la touche ( �tant un chiffre correspond � un caract�re du clavier )
	} while (!isTyping);
	return touche;
}

// La fonction sert a recuperer une direction en fonction du code ASCII de la touche pr�ss�es
int calculer_direction_touche(int touche) {

	/*D�CLARATION ET INITIALISATION DES VARIABLES*/
	int direction = 0;

	switch (touche) {
		// correspond a 'w'
	case 119:
		direction = 2;
		break;

		// correspond a 's'
	case 115:
		direction = 3;
		break;

		// correspond a 'a'
	case 97:
		direction = 1;
		break;

		// correspond a 'd'
	case 100:
		direction = 0;
		break;

		// par d�faut
	default:
		direction = -1;
		break;
	}

	return direction;
}

// La fonction sert a patienter un nombre de millisecondes donn�es en param�tre
void attendre(int millisecondes) {

	/*D�CLARATION ET INITIALISATION DES VARIABLES*/
	//On commence le chronom�tre
	int debut = clock();

	//On reste dans la boucle dans qu'on a pas atteint le nombre de millisecondes souhait�es
	while (clock() < debut + millisecondes);
}

// La fonction sert � attribuer 2 valeur al�atoire � posX et posY afin que notre serpent d�marre n'importe ou sur le terrain
void position_aleatoire(int NbLignes, int NbColonnes, int &posX, int &posY) {

	// On obtient une valeur random comprise entre 2 et le Nombre de ligne-1 pour l'axe des X qu'on attribue � posY
	// NBLignes-1 car on ne veut pas se retrouver sur une des bordures du terrain
	// 2 car on commence notre cadre � 1 caract�re de distance de l'ecran, le 2eme caract�re �tant le caract�re pour le cadre, on ne veut pas commencer ici
	posY = tirage_aleatoire(NbLignes - 1, 2);

	// On obtient une valeur random comprise entre 2et le Nombre de colonne pour l'axe des Y qu'on attribue � posX
	posX = tirage_aleatoire(NbColonnes - 1, 2);
}

// La fonction sert � donner de nouvelle valeur � posX et posY en fonction de la direction
void deplacer_serpent_I(int direction, int &posX, int &posY) {

	// Selon la direction donn� � la fonction, on va incrementer ou decrementer X ou Y
	// EX : le curseur est en (26,50), j'appuie sur la touche d ( direction = 0 ), posX augmente, je me trouve maintenant en (27,50)
	// EX : le curseur est en (31,78), j'appuie sur la touche s ( direction = 3 ), posY augmente, je me trouve maintenant en (31,79)

	gotoXY(posX, posY);
	cout << " ";

	// On se replace l� ou on avait �crit le X, et on �crit un espace � la place

	if (direction == 0)
		posX++;
	else if (direction == 1)
		posX--;
	else if (direction == 2)
		posY--;
	else if (direction == 3)
		posY++;

	gotoXY(posX, posY);
	cout << "X";
}

// La fonction sert � demander un niveau a l'utilisateur et a attribuer une valeur a la variable nbSouris en fonction de sa r�ponse
int saisir_niveau() {

	/*D�CLARATION ET INITIALISATION DES VARIABLES*/
	int difficulte, nbSouris;
	bool isAValidChoice = false;

	cout << "	NIVEAUX" << endl;
	cout << "	1. Debutant" << endl;
	cout << "	2. Facile" << endl;
	cout << "	3. Moyen" << endl;
	cout << "	4. Expert" << endl;

	do {
		cout << endl << "Choissisez votre niveau : ";
		cin >> difficulte;

		// la variable difficulte �tant un int, si l'utilisateur saisi des caract�res, cela mettra l'objet cin en mode �chec
		// On v�rifie si il est en mode �chec ( c a d si le tout dernier caract�re de la saisie n'est pas un retour � la ligne )
		if (cin.peek() != '\n') {
			// Si oui on remet l'objet en �tat normal
			cin.clear();

			// Et on vide son contenu
			cin.ignore(512, '\n');

			// Puis on affiche un message d'erreur
			cout << "ERREUR-01 : Ceci n'est pas un nombre !" << endl;
		}

		// On v�rifie �galement que la valeur rentr�e est bien comprise entre 1 et 4
		else if (difficulte < 1 || difficulte > 4)
			cout << "ERREUR-02 : Veuillez choisir entre 1 et 4 !" << endl;

		// Si aucun des test pr�c�dent n'est vrai, la variable n'est donc ni un caract�re, ni inf � 1, ni sup � 4, on peut donc la 
		// consid�rer comme �tant valide
		else
			isAValidChoice = true;

	} while (!isAValidChoice);

	// Au depart, le nombre de souris sera de 4 - difficulte sois
	// 5 souris pour le niveau 1
	// 10 souris pour le niveau 2
	// 15 souris pour le niveau 3
	// 20 souris pour le niveau 4
	nbSouris = 5 * difficulte;

	return nbSouris;
}

// La fonction sert a attribuer des coordon�es al�atoires � sourisX[] et sourisY[]
void creer_souris(int nbLignes, int NbColonnes, int sourisX[], int sourisY[], int nbSouris) {

	/*D�CLARATION ET INITIALISATION DES VARIABLES*/
	int i;

	for (i = 0; i < nbSouris; i++) {
		// On affecte sourisX[i] une valeur random entre 1 et 69 et a sourisY[i] une valeur random entre 1 et 29
		do {
			position_aleatoire(nbLignes, NbColonnes, sourisX[i], sourisY[i]);
		} while ((getCharXY(sourisX[i], sourisY[i]) != ' ') || (getCharXY(sourisX[i], sourisY[i]) != ' '));
	}
}

// La fonction sert a afficher des souris en fonction des coordon�es dans sourisX[] et sourisY[]
void afficher_souris(int sourisX[], int sourisY[], int nbSouris) {

	int i;
	// On modifie la couleur � 887 lors de la fonction d'affichage des souris
	color(887);
	
	// Puis on cr�e une boucle qui va aller a des coordon�es d�finies par sourisX et sourisY et y afficher un caract�re, un nombre nbSouris de fois.
	for (i = 0; i < nbSouris; i++) {
		gotoXY(sourisX[i], sourisY[i]);
		cout << "B";
	}

	//on modifie la couleur du jeu pour le vert
	color(2);
	
}


// Cette fonction sert � afficher le serpent au d�but du jeu
void afficher_serpent(int serpentX[], int serpentY[], int indiceTete, int indiceQueue){

	int i = 0;

	// On modifie la couleur du terrain a 2
	color(2);

	// On choisi des coordon�es al�atoire pour la t�te du serpent
	serpentX[indiceTete] = tirage_aleatoire(NB_COLONNES - 1, 0);
	serpentY[indiceTete] = tirage_aleatoire(NB_LIGNES - 1, 0);
	

	// On part du d�but du serpent jusqu'� sa fin ( en fonction des coordon�es du tableau serpentX[] et serpentY[] de d�but et de ses coordon�es de fin ) 
	// Et on affiche 'X' pour l'indice t�te, puis '*' pour le reste
	for (i = indiceTete; i <= indiceQueue; i++) {
		gotoXY(serpentX[indiceTete], serpentY[indiceTete]);
		cout << "X";
		gotoXY(serpentX[i], serpentY[i]);
		cout << "*";
	}

	// On modifie la couelur du terrain � 4
	color(4);
}

// La fonction sert � deplacer le serpent sur le terrain.
void deplacer_serpent_II(int direction, int serpentX[], int serpentY[], int &indiceTete, int &indiceQueue, int sourisX[], int sourisY[], int &nbSouris) {

	/*D�CLARATION ET INITIALISATION DES VARIABLES*/
	int i;

	// Au d�but de la fonction, on v�rifie si le serpent entre en collision avec une souris en regardant les coordon�es de sa t�te et en les comparants avec les
	// coordon�es du tableau de souris
	bool isCollidingMouse = tester_collision(serpentX[indiceTete], serpentY[indiceTete], sourisX, sourisY, nbSouris);

	// Si la r�ponse est vrai, on d�place plus loin l'indice de la queue du serpent
	if (isCollidingMouse == true) {
		indiceQueue++;
	}

	// En partant de la queue jusqu'a la t�te, on change de place chaque coordon�es des serpentX[] et serpentY[] contre sa pr�cedent
	for (i = indiceQueue; i > indiceTete; i--) {
		serpentX[i] = serpentX[i - 1];
		serpentY[i] = serpentY[i - 1];
	}

	// En fonction de la direction saisie par l'utilisateur, on va diminuer ou augmenter serpentX[] ou serpentY[]
	if (direction == 0)
		serpentX[indiceTete]++;
	else if (direction == 1)
		serpentX[indiceTete]--;
	else if (direction == 2)
		serpentY[indiceTete]--;
	else if (direction == 3)
		serpentY[indiceTete]++;

	// Si les coordon�es de la t�te du serpent �quivalent � '=' ou a 'l', alors on donne un �tat particuler � sa t�te, coder gr�ce au -1
	if ((getCharXY(serpentX[indiceTete], serpentY[indiceTete]) == '=') || (getCharXY(serpentX[indiceTete], serpentY[indiceTete]) == 'l'))
		indiceTete = -1;
	
	// On part du d�but du serpent jusqu'� sa fin ( en fonction des coordon�es du tableau serpentX[] et serpentY[] de d�but et de ses coordon�es de fin ) 
	// Et on affiche 'X' pour l'indice t�te, puis '*' pour le reste
	for (i = indiceTete; i <= indiceQueue; i++) {
		gotoXY(serpentX[indiceTete], serpentY[indiceTete]);
			cout << "X";
		gotoXY(serpentX[i], serpentY[i]);
			cout << "*";
	}

	// On efface le serpent apr�s chaque mouvement en se placant a l'indice de sa queue et en imprimant un espace � la fin de la fonction, apr�s le deplacement
	gotoXY(serpentX[indiceQueue], serpentY[indiceQueue]);
	cout << " ";

}

// Cette fonction renvoi un bool�en en verifiant si les coordon�es X Y de la t�te du serpent correspond � des coordon�es dans le tableau sourisX[] et sourisY[]
bool tester_collision(int X, int Y, int sourisX[], int sourisY[], int &nbSouris) {
	
	/*D�CLARATION ET INITIALISATION DES VARIABLES*/
	int i;
	bool isColliding = false;


	// On boucle de 0 jusqu'au nombre de souris
	for (i = 0; i <= nbSouris; i++) {

		// On verifie si X ( t�te du serpent ) ou Y �quivalent � des coordon�es dans le tableau des souris
		if ((sourisX[i] == X) && (sourisY[i] == Y)) {

			// Si oui, on modifie le bool�en a true
			isColliding = true;

			// On modifie le nombre total de souris
			nbSouris--;

			// On se place a l'indice de la souris a question dans le tableau sourisX[] et sourisY[] puis on modifie la valeur
			sourisX[i] = -1;
			sourisY[i] = -1;

		}
	}

	return isColliding;
}

//Fonction qui v�rifie si le serpent touche � un des mur du terrain de jeu
bool collision_mur(int X, int Y)
{
	/*D�CLARATION ET INITIALISATION DES VARIABLES*/
	bool isColliding = false;

	// On v�rifie si la tete du serpent se trouve aux coordon�es 0 ou NB_COLONNES ou NB_LIGNES ( c a d les coordon�es des murs du terrains )
	if (X == 0 || X == NB_COLONNES || Y == 0 || Y == NB_LIGNES) {

		// Si oui, on modifie le bool�en isColliding
		isColliding = true;
	}

	return isColliding;

}

//Fonction qui verifie si la t�te du serpent fait une collision avec son corps
bool collision_queue(int X, int Y, int serpentX[], int serpentY[], int indiceQueue)
{
	/*D�CLARATION ET INITIALISATION DES VARIABLES*/
	bool isColliding = false;

	// On boucle de 1 ( car on ne v�rifie pas la t�te ) jusqu'au l'indice de la queue
	for (int i = 1; i <= indiceQueue; i++) {

		// On test si les coordon�es de la t�te du serpent correspondent avec les cordoon�es de son corps
		if (serpentX[i] == X && serpentY[i] == Y) {
			isColliding = true;
		}
	}
	return isColliding;

}

// La fonction renvoi un delai calcul� selon le nombre de souris 
int calcul_delai(int nbSouris) {

	/*D�CLARATION ET INITIALISATION DES VARIABLES*/
	int delai;

	// La variable delai ne pourra descendre en de�a de 80, �tant donn� qu'elle d�finit le nombre de millisecondes que l'on doit attendre.
	delai = (nbSouris * 15) + 65;

	return delai;

}

//Fonction qui affiche le resultat final si le joueur gagne ou perd
void afficher_resultat_final(bool collisionMur, bool collisionQueue, int nbSouris)
{
	//Affiche 'GAME OVER' si un mur est touch� ou si le serpent se mange lui m�me
	if (collisionMur == true || collisionQueue == true) {
		cout << "XXXXX GAME OVER XXXXX" << endl;
	}
	//Affiche 'BRAVO' si toute les souris sont mang�e.
	else if (nbSouris <= 0) {
		cout << "***** BRAVO *****" << endl;
	}
}

void afficher_mur() {

	/*D�CLARATION ET INITIALISATION DES VARIABLES*/
	int i = 0, l = 0, j = 0;;
	int posX, posY;

	// On boucle 0 jusqu'� 10
	for (i = 0; i <= 10; i++) {

		// A chaque nombre paire 
		if (i % 2 == 0) {

			// On g�n�re des valeurs aleatoire pour posX ( de NB_COLONNES-4 jusqu'a 2 car on affiche horizontalement 4 caract�re, il faut etre sur de ne
			// pas toucher les bords )
			posX = tirage_aleatoire(NB_COLONNES-4, 2);
			posY = tirage_aleatoire(NB_LIGNES-1, 2);

			// On boucle de 0 jusqu'� 4
			for (l = 0; l < 4; l++) {

				// On se place aux coordon�es posX/posY et on augmente posX a chaque tour de boucle
				gotoXY(posX + l, posY);

				// En affichant un caract�re
				cout << '=';
			}
		}

		// Si le nombre est impair
		else{
			// On g�n�re des valeurs aleatoire pour posX ( de NB_LIGNES-4 jusqu'a 2 car on affiche verticalement 4 caract�re, il faut etre sur de ne
			// pas toucher les bords )
			posX = tirage_aleatoire(NB_COLONNES-1, 2);
			posY = tirage_aleatoire(NB_LIGNES-4, 2);

			// On boucle de 0 jusqu'� 4
			for (j = 0; j < 4; j++) {

				// On se place aux coordon�es posX/posY et on augmente posY a chaque tour de boucle
				gotoXY(posX, posY + j);

				// En affichant un caract�re
				cout << '|';
			}
		}
	}

	//on modifie la couleur du jeu pour le vert
	color(2);
}

// Cette fonction renvoi un nombre al�atoire entre min et max
int tirage_aleatoire(int max, int min) {

	return rand() % (max-min+1) + min;
}

int main() {

	/*D�CLARATION ET INITIALISATION DES VARIABLES*/
	int touche, direction;
	int posX = 0, posY = 0;
	int nbSouris = 0;
	int sourisX[40], sourisY[40];
	int serpentX[40] = {};
	int serpentY[40] = {};
	int indiceTete = 0;
	int indiceQueue = 1;
	int delai;
	bool isCollidingQueue = false;
	bool isCollidingMur = false;
	curseurVisible(false);

	// On initialise le seed du random a la fonction time()
	srand((unsigned int)time(NULL));

	// On affiche un menu a l'utilisateur afin qu'il choissise le niveau
	nbSouris = saisir_niveau();

	// On nettoie les informations de niveau pr�c�demment � l'�cran
	system("cls");

	// On affiche le terrain en fonction de deux constantes, NB_LIGNES et NB_COLONNES
	afficher_terrain(NB_LIGNES, NB_COLONNES);

	// On affiche le serpent en affectant 2 valeurs al�atoire a serpentX[indiceTete], serpentX[indiceQueue] et serpentY[indiceTete],serpentX[indiceQueue]
	afficher_serpent(serpentX, serpentY, indiceTete, indiceQueue);

	// On affiche les murs
	afficher_mur();

	// On cr�er des souris en envoyant NB_LIGNES, NB_COLONNES, sourisX, sourisY � la fonction ainsi que le nombre de souris.
	// Le tableau sourisX[] et sourisY[] contiendra des valeurs aleatoire qui seront utilis�es comme des coordon�es ult�rieurement
	creer_souris(NB_LIGNES, NB_COLONNES, sourisX, sourisY, nbSouris);

	// On envoi les tableau sourisX[] et sourisY[] a la fonction, qui s'occupera de positionner le curseur � (sourisX[i],	sourisY[i]) et a afficher 
	// un caract�re pour un nombre nbSouris de fois.
	afficher_souris(sourisX, sourisY, nbSouris);

	// on stock le retour de la fonction recuperer_touche dans une variable. Cette fonction renvoie un int qui correspond a un code ASCII
	touche = recuperer_touche();

	// On envoi la variable touche dans la fonction calculer_direction_touche() et on stock le resultat dans une variable.
	// Cette fonction renvoi un entier entre 0 et 3 ( 0 : droite ( pour a ), 1 : gauche ( pour d ), 2 : haut ( pour w ), 3 : bas ( pour s )
	direction = calculer_direction_touche(touche);

	do {
		
		delai = calcul_delai(nbSouris);
		// On attendre un nombre X de seconde avant de commencer la boucle
		attendre(delai);

		// Si l'utilisateur touche le clavier, on modifie la direction
		if (_kbhit()) {
			touche = recuperer_touche();
			direction = calculer_direction_touche(touche);
		}

		// On envoi la direction saisie par l'utlisateur, ainsi que les coordon�es X, Y a la fonction d�placer_serpent_I()
		// Cette fonction renvoi de nouvelles coordon�es pour posX et posY en fonction de la touche pr�ss� ( 'w' vers le haut, 'd' vers la droite etc. )
		//deplacer_serpent_I(direction, posX, posY);

		// On envoi la direction saisie par l'utlisateur, ainsi que les tableau serpentX et serpentY contenant les coordon�es du serpent
		// ainsi que l'indice de sa t�te et celui de sa queue.
		// Cette fonction modifie le tableau serpentX et serpentY a l'indice indiceTete en fonction de la direction choisie
		// puis affiche le serpent aux cordon�es (serpentX, serpentY)
		deplacer_serpent_II(direction, serpentX, serpentY, indiceTete, indiceQueue, sourisX, sourisY, nbSouris);

		//Verifie si la t�te du serpent fait une collision avec un des murs du terrain de jeu
		//Si la valeur retourn� est 'true', on sort de la boucle et le jeu se termine
		isCollidingMur = collision_mur(serpentX[indiceTete], serpentY[indiceTete]);
		//Verifie si la t�te du serpent fait une collision avec lui m�me
		//Si la valeur retourn� est 'true', on sort de la boucle et le jeu se termine.
		isCollidingQueue = collision_queue(serpentX[indiceTete], serpentY[indiceTete], serpentX, serpentY, indiceQueue);

		if (indiceTete < 0)
			isCollidingMur = true;

		/* POUR TESTER LE JEU, D�COMMENTER L'INCR�MENTATION DE LA VARIABLE VARIABLE COMPTE LE DEBUT DE LA BOUCLE WHILE { */
		/* POUR AUGMENTER LE NOMBRE DE DEPLACEMENT, MODIFIER LA VALEUR DE SORTIE DE LA BOUCLE ( 100 )*/
	} while (nbSouris > 1 && isCollidingMur == false && isCollidingQueue == false);

	//Affiche 'BRAVO' si toute les souris sont mang�e.
	//Affiche 'GAME OVER' si un mur est touch� ou si le serpent se mange lui m�me
	afficher_resultat_final(isCollidingMur, isCollidingQueue, nbSouris);
	cout << "***BRAVO***";
}